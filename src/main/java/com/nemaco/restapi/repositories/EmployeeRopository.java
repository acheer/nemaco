package com.nemaco.restapi.repositories;

import com.nemaco.restapi.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRopository extends JpaRepository<Employee, Long> {
}
