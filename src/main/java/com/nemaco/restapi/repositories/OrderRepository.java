package com.nemaco.restapi.repositories;

import com.nemaco.restapi.entities.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends JpaRepository<Invoice, Long> {



}
