package com.nemaco.restapi.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Product extends BaseEntity {

    private String name;

    private int price;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
            CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(
            name = "order_product",
            joinColumns = @JoinColumn(name = "product_id"),
            inverseJoinColumns = @JoinColumn(name = "order_id")
    )
    private List<Invoice> orderList;

    public Product() {
        super();
    }

    public Product(String name, int price, List<Invoice> orderList) {
        this.name = name;
        this.price = price;
        this.orderList = orderList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Invoice> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Invoice> orderList) {
        this.orderList = orderList;
    }

    public void addOrder(Invoice orders){
        if(this.orderList == null){
            orderList = new ArrayList<>();
        }

        orderList.add(orders);
    }
}
