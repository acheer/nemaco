package com.nemaco.restapi.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Customer extends BaseEntity {

    private String name;

    @OneToMany(mappedBy = "customer",
            cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    private List<Invoice> invoiceList;

    public Customer() {
        super();
    }

    public Customer(String name, List<Invoice> invoiceList) {
        super();
        this.name = name;
        this.invoiceList = invoiceList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public void addInvoice(Invoice invoices) {
        if (this.invoiceList == null) {
            invoiceList = new ArrayList<>();
        }

        invoiceList.add(invoices);
    }
}
