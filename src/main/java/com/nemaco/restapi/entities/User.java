package com.nemaco.restapi.entities;

import javax.persistence.Entity;

@Entity
public class User extends BaseEntity{

    // composite key needed type + ref Id

    private String username;

    private String password;

    private String authentication;

    // Define emp or dis
    private String type;

    private Long refId;

    public User(String username, String password, String authentication, String type, Long refId) {
        super();
        this.username = username;
        this.password = password;
        this.authentication = authentication;
        this.type = type;
        this.refId = refId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }
}
