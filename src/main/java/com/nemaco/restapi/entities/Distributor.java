package com.nemaco.restapi.entities;

import javax.persistence.Entity;

@Entity
public class Distributor extends BaseEntity{

    private String firstName;

    private String companyName;

    public Distributor() {
        super();
    }

    public Distributor(String firstName, String companyName) {
        super();
        this.firstName = firstName;
        this.companyName = companyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
