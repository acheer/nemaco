package com.nemaco.restapi.entities;

import javax.persistence.Entity;

@Entity
public class Employee extends BaseEntity {

    private String firstName;

    private String jobTitle;

    public Employee() {
        super();
    }

    public Employee(String firstName, String jobTitle) {
        super();
        this.firstName = firstName;
        this.jobTitle = jobTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
}
