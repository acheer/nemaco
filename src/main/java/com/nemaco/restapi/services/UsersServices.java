package com.nemaco.restapi.services;

import com.nemaco.restapi.entities.Employee;
import com.nemaco.restapi.repositories.EmployeeRopository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UsersServices {

    private EmployeeRopository employeeRopository;

    @Autowired
    public void setEmployeeRopository(EmployeeRopository employeeRopository) {
        this.employeeRopository = employeeRopository;
    }

    @GetMapping(path = "/nemaco/employee")
    public List<Employee> getAllEmployee(){
        return employeeRopository.findAll();
    }

    @GetMapping(path = "/nemaco/employee/{id}")
    public Employee getAllEmployeeById(@PathVariable long id){
        return employeeRopository.findById(id).get();
    }

    @PutMapping(path = "nemaco/employee/{id}")
    public ResponseEntity<Employee> updateEmployee(
            @PathVariable Long id,
            @RequestBody Employee employee
    ){
        Employee employeeUpdated = employeeRopository.save(employee);
        return new ResponseEntity<>(employeeUpdated, HttpStatus.OK);
    }

    @PostMapping(path = "/nemaco/employee")
    public ResponseEntity<Void> createEmployee(
            @RequestBody Employee employee
    ){
        Employee createdEmployee = employeeRopository.save(employee);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdEmployee.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(path = "/nemaco/employee/{id}")
    public ResponseEntity<Void> deleteEmployee(
            @PathVariable Long id
    ){
        employeeRopository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
