package com.nemaco.restapi.services;

import com.nemaco.restapi.entities.Product;
import com.nemaco.restapi.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ProductServices {

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping(path = "/nemaco/product")
    public List<Product> getAllProduct(){
        return productRepository.findAll();
    }

    @GetMapping(path = "/nemaco/product/{id}")
    public Product getProductById(@PathVariable Long id){
        return productRepository.findById(id).get();
    }

    @PostMapping(path = "/nemaco/product")
    public ResponseEntity<Void> createProduct(@RequestBody Product product){
        Product createdProduct = productRepository.save(product);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdProduct.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping(path = "/nemaco/product/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id){
        productRepository.deleteById(id);

        return ResponseEntity.noContent().build();
    }

    @PutMapping(path = "/nemaco/product/{id}")
    public ResponseEntity<Product> updateProduct(
            @PathVariable Long id,
            @RequestBody Product product
    ){
        Product updatedProduct = productRepository.save(product);
        return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
    }

}
